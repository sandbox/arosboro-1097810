Installing the Vtiger Portal Module

Download the module and place it in sites/all/modules.  Enable the module in
the modules section of the site.

You may want to consider setting the Registration and Cancellation settings in 
Configuration > Account Settings to Administrators only so that non portal 
users can't sign up.
