<?php

/**
 * @file
 * Administration page callbacks for the vtiger portal module.
 */

/**
 * Form builder. Configure vtiger portal.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function vtiger_portal_admin_settings() {
  $form['vtiger_portal_server_path'] = array(
    '#title' => t('vtiger Server Path'),
    '#type' => 'textfield',
    '#description' => t('This is the vtiger server path ie., the url to access the vtiger server in browser. No trailing slash.<br />Ex. http://domainname/vtiger'),
    '#default_value' => variable_get('vtiger_portal_server_path', ''),
  );
  $form['vtiger_portal_proxy_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Proxy Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['vtiger_portal_proxy_settings']['proxy_host'] = array(
    '#title' => t('Proxy Host'),
    '#type' => 'textfield',
    '#description' => t('Host name of the proxy.'),
    '#default_value' => variable_get('vtiger_portal_proxy_settings', ''),
  );
  $form['vtiger_portal_proxy_settings']['proxy_host'] = array(
    '#title' => t('Proxy Host'),
    '#type' => 'textfield',
    '#description' => t('Host name of the proxy.'),
    '#default_value' => variable_get('vtiger_portal_proxy_host', ''),
  );
  $form['vtiger_portal_proxy_settings']['proxy_port'] = array(
    '#title' => t('Proxy Port'),
    '#type' => 'textfield',
    '#description' => t('Port number of the proxy.'),
    '#default_value' => variable_get('vtiger_portal_proxy_port', ''),
  );
  $form['vtiger_portal_proxy_settings']['proxy_username'] = array(
    '#title' => t('Proxy Username'),
    '#type' => 'textfield',
    '#description' => t('User name of the proxy.'),
    '#default_value' => variable_get('vtiger_portal_proxy_username', ''),
  );
  $form['vtiger_portal_proxy_settings']['proxy_password'] = array(
    '#title' => t('Proxy Password'),
    '#type' => 'password',
    '#description' => t('Password of the proxy.'),
    '#default_value' => variable_get('vtiger_portal_proxy_password', ''),
  );

  $form['#submit'][] = 'vtiger_portal_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Process vtiger portal settings submission.
 */
function vtiger_portal_admin_settings_submit($form, $form_state) {
  variable_set('vtiger_portal_server_path', 
    $form_state['values']['vtiger_portal_server_path']);
  variable_set('vtiger_portal_proxy_host',
    $form_state['values']['proxy_host']);
  variable_set('vtiger_portal_proxy_port',
    $form_state['values']['proxy_port']);
  variable_set('vtiger_portal_proxy_username',
    $form_state['values']['proxy_username']);
  variable_set('vtiger_portal_proxy_password',
    $form_state['values']['proxy_password']);

}

/**
 * Validate vtiger portal settings submission.
 */
function vtiger_portal_admin_settings_validate($form, $form_state) {
}
